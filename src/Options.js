import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();

class Options extends Component {


      componentDidMount()
      {
        var CookieDate = new Date();
        CookieDate.setFullYear(CookieDate.getFullYear() +10);
        var expdate = CookieDate.toGMTString();
        
         jQuery("#any_card").click(function() 
         {
        cookies.set('cookieanycard', {key: "anycard"}, { path: '/' }, { expires: expdate })
         });

         jQuery("#aadhar_card").click(function() 
         {
        cookies.set('cookieanycard', {key: "aadharcard"}, { path: '/' }, { expires: expdate })
         });

         jQuery("#pan_card").click(function() 
         {
        cookies.set('cookieanycard', {key: "pancard"}, { path: '/' }, { expires: expdate })
         });
         }

    render() {
        return ( 
            <div className = "Options">
            <Header />
    <div className="container">
     <div className="intro">
     <h5>Please Select the options</h5>
     <Row className="select-options">
         <Col></Col>
         <Col>
             <div className="select-list">
                <p><a href="/#/uploadkyc" id="any_card">Any KYC Document</a></p>
             </div>
              <div className="select-list">
                 <p><a href="/#/uploadkyc" id="aadhar_card"> Aadhar Card </a></p>
             </div>
              <div className="select-list">
                  <p><a href="/#/uploadkyc" id="pan_card"> PAN Card </a></p>
             </div>
        </Col>

             <Col></Col>
             </Row>
     </div>
            </div>
            </div>
        );
    }
}

export default Options;