import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import Camera, {
    FACING_MODES,
    IMAGE_TYPES
} from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col,
    Input,
    Button
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import loader from './loader.gif';
import Header from './Header';

class App extends Component {
    constructor(props) {
        super(props);
    }

    

    render() {
        return ( 
            <div className = "App">
            <Header />
    <div className="container">
     <div className="intro">
             <h6>KYC Verification API will allow customers to seamlessly verify the type of identifications users provide 
             – Aadhaar, PAN and Passport to name a few.</h6>
             <div className="home-para">
             <a href="https://documenter.getpostman.com/view/6254805/SWLcc8Ns?version=latest" target="_blank"> API Documentation </a>
             </div>

             <div>
              <Button id="createlist"> <a href="/#/options">Select the option and submit required documents here</a> </Button>
             </div>
             
     </div>
            </div>
            </div>
        );
    }
}

export default App;