import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Uploadselfie from './Uploadselfie';
import List from './List';
import Show from './Show';
import Uploadkyc from './Uploadkyc';
import EditAPI from './EditAPI';
import EditList from './EditList';
import Details from './Details';
import Options from './Options';
import { HashRouter, Route, BrowserRouter as Router } from 'react-router-dom';


import * as serviceWorker from './serviceWorker';

const routing = (
    <HashRouter>
    <div>
     
      <Route exact path="/" component={App} />
      <Route path="/uploadselfie" component={Uploadselfie} />
      <Route path="/list" component={List} />
      <Route path="/show" component={Show} />
      <Route path="/uploadkyc" component={Uploadkyc} />
      <Route path="/editapi" component={EditAPI} />
      <Route path="/editlist" component={EditList} />
      <Route path="/details" component={Details} />
      <Route path="/options" component={Options} />
     
    </div>
  </HashRouter>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
