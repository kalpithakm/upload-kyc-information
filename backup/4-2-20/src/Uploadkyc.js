import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import Camera, {
    FACING_MODES,
    IMAGE_TYPES
} from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
import config from './config';
const cookies = new Cookies();
var APIkey;
var SelectedCard;

class Uploadkyc extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

  this.state = {
        apikey: cookies.get('cookieAPIkey'),
        selectcard: cookies.get('cookieanycard')
     }

     var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
  
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

   var selectcardCookie = cookies.get('cookieanycard')

    if (selectcardCookie != null) 
    {
  var xy = cookies.get('cookieanycard');
   SelectedCard = xy.key;
  }
  }

    uploadSelfie(e) {
       
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;

                        jQuery.ajax({
                            url: config.upload_kyc_api,
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                 attributes: "text",
                                 imageCrops: "front,front_top,back,front_bottom,photo,signature,qrcode"
                              
                            }),

                            success: function(response) {

                                jQuery("#error-message").hide();
                               

                                if (response) {
                                    console.log(response);
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";
                                    document.getElementById("front_back").style.display = "none";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    

                                    if (response.result) 
                                      {
                                        if (response.result.length === 1) 
                                      {
                                    var documentid = response.result[0].documentId;
                                    
                                    var CookieDate = new Date();
                                    CookieDate.setFullYear(CookieDate.getFullYear() +10);
                                    var expdate = CookieDate.toGMTString();
                                    
                                    cookies.set('cookieDocumentid', {key: documentid}, { path: '/' }, { expires: expdate })
                                    
                                    if (response.result[0].type === "aadhaar" || response.result[0].type === "pan")
                                    {
                                        document.getElementById("success-message-1").innerHTML =
                                    "<h5>Your document uploaded successfully, Please follow next step to match with photo..</h5>";
                                    var doc_id = response.result[0].documentId;
                                    
                                     jQuery('#card_doc_id').html(doc_id);

                                      var jString = JSON.stringify(response.result[0].imageCrops);
                                      var jdata = JSON.parse(jString);
                                      var trHTML = '';
                                          jQuery.each(jdata, function(key, value)
                                          {
                                            trHTML += 
                                           '<tr><td>' + key +  
                                           '</td><td>' + '<img  src= '+ value +'  />' +
                                           '</td></tr>';       
                                         });
                                     jQuery('#kyc-document-image-table').append(trHTML);  
                                     
                                    }
                                    else
                                    {
                                         document.getElementById("success-message-2").innerHTML =
                                    "<h5>ID Not Matched, Please Upload the valid photo copy of Aadhar Card or PAN Card.</h5>";
                                     document.getElementById("typematch").style.display = "block";
                                     document.getElementById("responsetab").style.display = "none";
                                    document.getElementById("next-step").style.display = "none";
                                    }

                                     if (response.result[0].type === "aadhaar") 
                                      {
                                      document.getElementById("records_pan").style.display = "none";
                                    var c_type = response.result[0].type;
                                     
                                     var c_aadhaarno = response.result[0].details.aadhaar;
                                    
                                     var c_dob = response.result[0].details.dob;
                                    
                                     var c_doi = response.result[0].details.doi;
                                    
                                     var c_father = response.result[0].details.father;
                                    
                                     var c_husband = response.result[0].details.husband;
                                    
                                     var c_gender = response.result[0].details.gender;
                                   
                                     var c_mother = response.result[0].details.mother;
                                     
                                     var c_name = response.result[0].details.name;
                                    
                                     var c_yob = response.result[0].details.yob;
                                    
                                     var c_qr = response.result[0].details.qr;
                                    
                                     var c_addresscareof = response.result[0].details.address.care_of;
                                    
                                     var c_district = response.result[0].details.address.district;
                                     
                                     var c_sub_district = response.result[0].details.address.sub_district;
                                     
                                     var c_locality = response.result[0].details.address.locality;
                                     
                                      var c_landmark = response.result[0].details.address.landmark;
                                     
                                      var c_street = response.result[0].details.address.street;
                                     
                                      var c_vtc = response.result[0].details.address.vtc;
                                     
                                     var c_house = response.result[0].details.address.house;
                                     
                                     var c_pin = response.result[0].details.address.pin;
                                     
                                     var c_post_office = response.result[0].details.address.post_office;
                                     
                                     var c_state = response.result[0].details.address.state;
                                     
                                     var c_cvalue = response.result[0].details.address.value;
                                     
                                     var c_phone = response.result[0].details.phone;
         
                                       jQuery('#d_type').html(c_type);
                                       jQuery('#d_cardnumber').html(c_aadhaarno);
                                       jQuery('#d_dob').html(c_dob);
                                       jQuery('#d_doi').html(c_doi);
                                       jQuery('#d_father').html(c_father);
                                       jQuery('#d_husband').html(c_husband);
                                       jQuery('#d_gender').html(c_gender);
                                       jQuery('#d_mother').html(c_mother);
                                       jQuery('#d_name').html(c_name);
                                       jQuery('#d_yob').html(c_yob);
                                       jQuery('#d_qr').html(c_qr);
                                       jQuery('#d_addr_careof').html(c_addresscareof);
                                       jQuery('#d_addr_district').html(c_district);
                                       jQuery('#d_addr_sub_district').html(c_sub_district);
                                       jQuery('#d_addr_locality').html(c_locality);
                                       jQuery('#d_addr_landmark').html(c_landmark);
                                       jQuery('#d_addr_street').html(c_street);
                                       jQuery('#d_addr_vtc').html(c_vtc);
                                       jQuery('#d_addr_house').html(c_house);
                                       jQuery('#d_addr_pin').html(c_pin);
                                       jQuery('#d_addr_postoffice').html(c_post_office);
                                       jQuery('#d_addr_state').html(c_state);
                                       jQuery('#d_addr_value').html(c_cvalue);
                                       jQuery('#d_phonenumber').html(c_phone);
                                        
                                        }

                                         if (response.result[0].type === "pan") 
                                      {
                                        document.getElementById("records_aadhar").style.display = "none";
                                     var p_type = response.result[0].type;
                                     
                                     var p_panno = response.result[0].details.pan_no;
                                    
                                     var p_dob = response.result[0].details.date;
                                    
                                     var p_name = response.result[0].details.name;
                                     
                                     var p_father = response.result[0].details.father;
                                     
                                     var p_dateofissue = response.result[0].details.date_of_issue;
                                     
                                                   jQuery('#pan_type').html(p_type);
                                     jQuery('#pan_number').html(p_panno);
                                     jQuery('#pan_dob').html(p_dob);
                                     jQuery('#pan_name').html(p_name);
                                     jQuery('#pan_father').html(p_father);
                                      jQuery('#pan_dateisssue').html(p_dateofissue);

                                      }

                                      if (response.result[0].type === "passport_front") 
                                      {
                                       document.getElementById("success-message-passport").innerHTML =
                                    "<h5>Passport document is in development process..</h5>";
                                     document.getElementById("success-message-2").style.display = "none";
                                     document.getElementById("responsetab").style.display = "none";
                                      }

                                      }
                                  }                 
                                   
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                document.getElementById("front_back").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }


componentDidMount()
      {
    if(SelectedCard === "anycard")
  {
  document.getElementById("page_title_anycard").style.display = "block";
  }

  if(SelectedCard === "aadharcard")
  {
  document.getElementById("page_title_aadharcard").style.display = "block";
  }

  if(SelectedCard === "pancard")
  {
  document.getElementById("page_title_pancard").style.display = "block";
  }

   jQuery("#doc-image-link").click(function() 
   {
     document.getElementById("doc-image-link").style.backgroundColor = "#f3f3f3";
     document.getElementById("doc-text-link").style.backgroundColor = "#fff";
      document.getElementById("responsecode-image-tab").style.display = "block";
      document.getElementById("responsecode-text-tab").style.display = "none";
   });

   jQuery("#doc-text-link").click(function() 
   { 
    document.getElementById("doc-text-link").style.backgroundColor = "#f3f3f3";
    document.getElementById("doc-image-link").style.backgroundColor = "#fff";
      document.getElementById("responsecode-image-tab").style.display = "none";
      document.getElementById("responsecode-text-tab").style.display = "block";
   });
}

    render() {
        return ( 
            <div className = "Uploadkyc" >
            <Header />

            <div className ="page-title">
            <h5 id="page_title_anycard" style={{ display: "none" }}>Upload Aadhar or PAN Card </h5>
            <h5 id="page_title_aadharcard" style={{ display: "none" }}>Upload Aadhar Card </h5>
            <h5 id="page_title_pancard" style={{ display: "none" }}>Upload PAN Card</h5>
            </div>

            <div id = "upload-photo-section">
            <div className = "ios-pic">
            <div >
            <img className = "display-img"
            id = "pic"
            src = ""
            alt = ""/>
            <img className = "display-img-url"
            id = "photo"
            src = ""
            alt = ""/>
            </div>

            <div className = "upload-loader" > </div>

            <div >
            <label className = "fileContainer">
            Upload a document <
            input id = "browse-file"
            type = "file"
            onClick = {
                this.uploadSelfie
            }
            /> 
            </label>

            </div>

            </div> </div>

            <div className ="goback" id="front_back">
               <span className = "try-tab success-btn">
                     <a href="/#/options">Back</a>
                </span>
            </div>

            <div>
            <p id = "error-message"> </p>
            </div>

            <div>
                 <p id="success-message"></p>
            </div>
             <div>
                 <p id="success-message-1"></p>
            </div>
            <div>
                 <p id="success-message-2"></p>
            </div>

            <div>
                 <p id="success-message-passport"></p>
            </div>

            <div id="responsetab" style={{ display: "none" }}>

            <Row className="refresh-btn">
         <Col></Col>
         <Col id="next-step">
            <div>
               <span className = "try-tab primary-btn">
                     <a href="/#/uploadselfie">Next: Match With Photo</a>
                </span>
              </div>
               <div className ="goback">
               <span className = "try-tab success-btn">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Back</a>
                </span>
              </div>
               
             </Col>
             <Col></Col>
            </Row>

             <div>
              <h5> Document Id (<span id="card_doc_id"></span>)</h5>
             </div>
            
            <div id="doc-link-section">
                <div id="doc-text-link">
                Document Text Details
                </div>

               <div id="doc-image-link"> 
               Document Image Section
               </div>
             </div>

           <div id="responsecode-image-tab" style={{ display: "none" }}>
             <Row>
               <Col> </Col>

               <Col>
               <p></p>
               <h5>Document Image Section</h5>
               <p></p>
               <table id="kyc-document-image-table">
                </table>
               </Col>

                <Col> </Col>

                </Row>
                </div>

            <div id="responsecode-text-tab" >
        <Row>
        <Col> </Col>
        <Col>
               <p></p>
               <h5> Document Text Details  </h5>
               <p></p>
        <table className="table aadhaar" id="records_aadhar" border='1'>

               <tr>
               <td>Card Type</td>
               <td id="d_type"></td>
               </tr>

               <tr>
               <td>Card Number</td>
               <td id="d_cardnumber"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="d_dob"></td>
               </tr>

               <tr>
               <td>DOI</td>
               <td id="d_doi"></td>
               </tr>

               <tr>
               <td>Father</td>
               <td id="d_father"></td>
               </tr>

               <tr>
               <td>Husband</td>
               <td id="d_husband"></td>
               </tr>

               <tr>
               <td>Gender</td>
               <td id="d_gender"></td>
               </tr>

               <tr>
               <td>Mother</td>
               <td id="d_mother"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="d_name"></td>
               </tr>

               <tr>
               <td>YOB</td>
               <td id="d_yob"></td>
               </tr>

               <tr>
               <td>QR</td>
               <td id="d_qr"></td>
               </tr>

               <tr>
               <td>Address care of</td>
               <td id="d_addr_careof"></td>
               </tr>

               <tr>
               <td>Address district</td>
               <td id="d_addr_district"></td>
               </tr>

               <tr>
               <td>Address sub district</td>
               <td id="d_addr_sub_district"></td>
               </tr>

               <tr>
               <td>Address locality</td>
               <td id="d_addr_locality"></td>
               </tr>

               <tr>
               <td>Address landmark</td>
               <td id="d_addr_landmark"></td>
               </tr>

               <tr>
               <td>Address street</td>
               <td id="d_addr_street"></td>
               </tr>

               <tr>
               <td>Address vtc</td>
               <td id="d_addr_vtc"></td>
               </tr>

               <tr>
               <td>Address house</td>
               <td id="d_addr_house"></td>
               </tr>

               <tr>
               <td>Address pin</td>
               <td id="d_addr_pin"></td>
               </tr>

               <tr>
               <td>Address post office</td>
               <td id="d_addr_postoffice"></td>
               </tr>

               <tr>
               <td>Address state</td>
               <td id="d_addr_state"></td>
               </tr>

               <tr>
               <td>Address value</td>
               <td id="d_addr_value"></td>
               </tr>

               <tr>
               <td>Phone number</td>
               <td id="d_phonenumber"></td>
               </tr>

              </table>

             <table className="table pan" id="records_pan" border='1'>
              
               <tr>
               <td>Card type</td>
               <td id="pan_type"></td>
               </tr>

               <tr>
               <td>Card Number</td>
               <td id="pan_number"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="pan_dob"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="pan_name"></td>
               </tr>

               <tr>
               <td>Father</td>
               <td id="pan_father"></td>
               </tr>

                <tr>
               <td>Card date of issue</td>
               <td id="pan_dateisssue"></td>
               </tr>

               </table>
               </Col>

                <Col> </Col>
             </Row>
             </div>

        <Row className="responsecode-tab">
            <Col></Col>
            <Col>
                <pre id="responsecode" style={{ display: "none" }} className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        
    </div>

<div id="typematch" style={{ display: "none" }}>
   <Row className="refresh-btn">
         <Col></Col>
         <Col>
             <span className = "try-tab">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Back</a>
                 </span>
          </Col>
          <Col></Col>
         </Row>
 </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col>
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

            <div id = "try-btn"
            style = {
                {
                    display: "none"
                }
            } >
            <span className = "try-tab">
            <a href = "/" > Try Again </a> 
            </span> </div>
</div>
        );
    }
}

export default Uploadkyc;