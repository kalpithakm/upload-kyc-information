import React, { Component } from "react";
import "./App.css";
import "./custom.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {  Row, Col } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var CardDetails, CardDetails_ctype;

class Details extends Component {
  constructor(props) {
    super(props);

      this.state = {
        CardtypeResponse: cookies.get('cookieCardResponse_ctype'),
        CardResponse: cookies.get('cookieCardResponse')

     }


     var mycardresponseCookie = cookies.get('cookieCardResponse')

    if (mycardresponseCookie != null) 
    {
  var x = cookies.get('cookieCardResponse');
   CardDetails = x.key;
   console.log("cookieCardResponse", CardDetails);
  }
   else
   {
     alert("Please upload the kyc");
   }


 var mycardtypeCookie = cookies.get('cookieCardResponse_ctype')

    if (mycardtypeCookie != null) 
    {
  var xy = cookies.get('cookieCardResponse_ctype');
   CardDetails_ctype = xy.key;
   console.log("cookieCardResponse_ctype", CardDetails_ctype);
  }
   else
   {
     alert("Please upload the kyc");
   }
}



  onCameraError(error) {
    console.error("onCameraError");
    document.getElementById("video-section").style.display = "none";
  }

  onCameraStart(stream) {
    console.log("onCameraStart");
  }

  onCameraStop() {
    console.log("onCameraStop");
  }

  componentDidMount()
  {
    jQuery(document).ready(function() {

       jQuery('#responsecode').html(CardDetails);
       jQuery('#d_type').html(CardDetails_ctype);
     });

  }


  render() {
    return (
      <div className="Details">
        <Header />
        
        <div id="identify-namepath">
        <Row>
           <Col>
           </Col>

           <Col>
          
           <div className="idn-list" id="idn-tab">

           
            


             </div>
                 </Col>

                 <Col>
                </Col>

                </Row>
        </div>


          <div id="video-section" style={{ display: "none" }}>
          <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 500 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.50}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />
       
         <div className="loader"> </div>

          <div id="tips">
          <p>
           Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears.          
          </p>
     
          </div>
          <div id="options-or">
            <p className="or">OR</p>
          </div>
        </div>

       


         <div id="responsetab" >
        <Row>
            <Col></Col>
            <Col>
            <table className="table" id="records_table" border='1'>
               
               <tr>
               <td>Card Type</td>
               <td id="d_type"></td>
               </tr>

               <tr>
               <td>Card Number</td>
               <td id="d_cardnumber"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="d_dob"></td>
               </tr>

               <tr>
               <td>DOI</td>
               <td id="d_doi"></td>
               </tr>

               <tr>
               <td>Father</td>
               <td id="d_father"></td>
               </tr>

               <tr>
               <td>Husband</td>
               <td id="d_husband"></td>
               </tr>

               <tr>
               <td>Gender</td>
               <td id="d_gender"></td>
               </tr>

               <tr>
               <td>Mother</td>
               <td id="d_mother"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="d_name"></td>
               </tr>

               <tr>
               <td>YOB</td>
               <td id="d_yob"></td>
               </tr>

               <tr>
               <td>QR</td>
               <td id="d_qr"></td>
               </tr>

               <tr>
               <td>Address care of</td>
               <td id="d_addr_careof"></td>
               </tr>

               <tr>
               <td>Address district</td>
               <td id="d_addr_district"></td>
               </tr>

               <tr>
               <td>Address sub district</td>
               <td id="d_addr_sub_district"></td>
               </tr>

               <tr>
               <td>Address locality</td>
               <td id="d_addr_locality"></td>
               </tr>

               <tr>
               <td>Address landmark</td>
               <td id="d_addr_landmark"></td>
               </tr>

               <tr>
               <td>Address street</td>
               <td id="d_addr_street"></td>
               </tr>

               <tr>
               <td>Address vtc</td>
               <td id="d_addr_vtc"></td>
               </tr>

               <tr>
               <td>Address house</td>
               <td id="d_addr_house"></td>
               </tr>

               <tr>
               <td>Address pin</td>
               <td id="d_addr_pin"></td>
               </tr>

               <tr>
               <td>Address post office</td>
               <td id="d_addr_postoffice"></td>
               </tr>

               <tr>
               <td>Address state</td>
               <td id="d_addr_state"></td>
               </tr>

               <tr>
               <td>Address value</td>
               <td id="d_addr_value"></td>
               </tr>

               <tr>
               <td>Phone number</td>
               <td id="d_phonenumber"></td>
               </tr>

               <tr>
               <td>Tag</td>
               <td id="d_tag"></td>
               </tr>




              </table>

                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn" style={{ display: "none" }}>
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/list" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>
    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/list" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

       
      </div>
    );
  }
}

export default Details;