import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import {
    Button
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import Header from './Header';

class App extends Component {

    render() {
        return ( 
            <div className = "App">
            <Header />
    <div className="container">
     <div className="intro">
             <h6>KYC Verification API will allow customers to seamlessly verify the type of identifications users provide 
             – Aadhaar, PAN and Passport to name a few.</h6>
             <div className="home-para">
             <a href="https://documenter.getpostman.com/view/6717107/SVtZuRFd?version=latest" rel="noopener noreferrer" target="_blank"> API Documentation </a>
             </div>

             <div>
              <Button id="createlist"> <a href="/#/options">Select the option and submit required documents here</a> </Button>
             </div>
             
     </div>
            </div>
            </div>
        );
    }
}

export default App;