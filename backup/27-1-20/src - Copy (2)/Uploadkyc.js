import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import Camera, {
    FACING_MODES,
    IMAGE_TYPES
} from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col,
    Input,
    Button
} from "reactstrap";
import {
    Link
} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";
import loader from './loader.gif';
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var APIkey;
var SelectedCard;

class Uploadkyc extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

  this.state = {
        apikey: cookies.get('cookieAPIkey'),
        selectcard: cookies.get('cookieanycard')
     }

     var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   console.log("APIkey", APIkey);
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

   var selectcardCookie = cookies.get('cookieanycard')

    if (selectcardCookie != null) 
    {
  var x = cookies.get('cookieanycard');
   SelectedCard = x.key;
   console.log("cookieanycard", SelectedCard);
  }
  }


    onTakePhoto(dataUri) {
        // Do stuff with the dataUri photo...
        
        console.log("takePhoto");
        jQuery.ajax({
            url: "https://southeastasia.cognitive.sparshik.com/api/v1/kyc/detect/",
            method: "POST",
            crossDomain: true,
            headers: {
                Authorization: APIkey,
                "Content-Type": "application/json"
            },
            data: JSON.stringify({
                dataUri: dataUri,
                 attributes: "text"
            }),
            success: function(response) {
                jQuery("#error-message").hide();
                console.log(response);

                if (response) 
                {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";
                                    document.getElementById("front_back").style.display = "none";

                  var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    

                                    if (response.result) 
                                      {
                                        if (response.result.length === 1) 
                                      {
                                    var documentid = response.result[0].documentId;
                                    console.log("documentid",documentid);
                                    var CookieDate = new Date;
                                    CookieDate.setFullYear(CookieDate.getFullYear() +10);
                                    var expdate = CookieDate.toGMTString();
                                     console.log(expdate);
                                    cookies.set('cookieDocumentid', {key: documentid}, { path: '/' }, { expires: expdate })
                                    var aadhaar, pan;
                                    var document_type = response.result[0].type;
                                    console.log("document-type",document_type);

                                    if (response.result[0].type === "aadhaar" || response.result[0].type === "pan")
                                    {
                                        document.getElementById("success-message-1").innerHTML =
                                    "<h5>Your ID Matched, Please follow the next step..</h5>";
                                    }
                                    else
                                    {
                                         document.getElementById("success-message-2").innerHTML =
                                    "<h5>ID Not Matched, Please Upload the valid photo copy of Aadhar Card or PAN Card.</h5>";
                                     document.getElementById("typematch").style.display = "block";
                                    document.getElementById("next-step").style.display = "none";
                                    }

                                      }
                                  }                 
                                   
                                }
                            },

            beforeSend: function() {
                jQuery('.loader').show();
                jQuery('#container-circles').hide();
                jQuery('.react-html5-camera-photo').hide();

            },
            complete: function() {
                jQuery('.loader').hide();
                jQuery('#container-circles').show();
                jQuery('.react-html5-camera-photo').show();
            },

             error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("video-section").style.display = "none";
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("tips").style.display = "none";
                                document.getElementById("options-or").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                document.getElementById("front_back").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }
        });
    }

    uploadSelfie(e) {
        document.getElementById("video-section").style.display = "none";
        document.getElementById("tips").style.display = "none";
        document.getElementById("options-or").style.display = "none";

        /* document.getElementById("upload-photo-section").style.display = "block";*/
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;

                        jQuery.ajax({
                            url: "https://southeastasia.cognitive.sparshik.com/api/v1/kyc/detect/",
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                 attributes: "text"
                              
                            }),

                            success: function(response) {

                                jQuery("#error-message").hide();
                                console.log(response);

                                if (response) {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";
                                    document.getElementById("front_back").style.display = "none";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    

                                    if (response.result) 
                                      {
                                        if (response.result.length === 1) 
                                      {
                                    var documentid = response.result[0].documentId;
                                    console.log("documentid",documentid);
                                    var CookieDate = new Date;
                                    CookieDate.setFullYear(CookieDate.getFullYear() +10);
                                    var expdate = CookieDate.toGMTString();
                                     console.log(expdate);
                                    cookies.set('cookieDocumentid', {key: documentid}, { path: '/' }, { expires: expdate })
                                    var aadhaar, pan;
                                    var document_type = response.result[0].type;
                                    console.log("document-type",document_type);

                                    if (response.result[0].type === "aadhaar" || response.result[0].type === "pan")
                                    {
                                        document.getElementById("success-message-1").innerHTML =
                                    "<h5>Your document uploaded successfully, Please follow next step to upload selfie..</h5>";
                                    }
                                    else
                                    {
                                         document.getElementById("success-message-2").innerHTML =
                                    "<h5>ID Not Matched, Please Upload the valid photo copy of Aadhar Card or PAN Card.</h5>";
                                     document.getElementById("typematch").style.display = "block";
                                    document.getElementById("next-step").style.display = "none";
                                    }

                                      }
                                  }                 
                                   
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("tips").style.display = "none";
                                document.getElementById("options-or").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                document.getElementById("front_back").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }

    onCameraError(error) {
        console.error("onCameraError");
        document.getElementById("video-section").style.display = "none";

    }

    onCameraStart(stream) {
        console.log("onCameraStart");
    }

    onCameraStop() {
        console.log("onCameraStop");
    }

componentDidMount()
      {
    if(SelectedCard === "anycard")
  {
  document.getElementById("page_title_anycard").style.display = "block";
  }

  if(SelectedCard === "aadharcard")
  {
  document.getElementById("page_title_aadharcard").style.display = "block";
  }

  if(SelectedCard === "pancard")
  {
  document.getElementById("page_title_pancard").style.display = "block";
  }
}

    render() {
        return ( 
            <div className = "Uploadkyc" >
            <Header />

            <div className ="page-title">
            <h5 id="page_title_anycard" style={{ display: "none" }}>Upload Aadhar or PAN Card / Take a Photo of Aadhar or PAN Card</h5>
            <h5 id="page_title_aadharcard" style={{ display: "none" }}>Upload Aadhar Card / Take a Photo of Aadhar Card</h5>
            <h5 id="page_title_pancard" style={{ display: "none" }}>Upload PAN Card / Take a Photo of PAN Card</h5>
            </div>

            <div id = "video-section" >
            <Camera onTakePhoto = {
                dataUri => {
                    this.onTakePhoto(dataUri);
                }
            }
            onCameraError = {
                error => {
                    this.onCameraError(error);
                }
            }
            idealFacingMode = {
                FACING_MODES
            }
            idealResolution = {
                {
                    width: 900,
                    height: 500
                }
            }
            imageType = {
                IMAGE_TYPES.JPG
            }
            imageCompression = {
                0.50
            }
            isMaxResolution = {
                false
            }
            isImageMirror = {
                false
            }
            isDisplayStartCameraError = {
                true
            }
            sizeFactor = {
                1
            }
            onCameraStart = {
                stream => {
                    this.onCameraStart(stream);
                }
            }
            onCameraStop = {
                () => {
                    this.onCameraStop();
                }
            }
            />

            <div className = "loader"> </div>

            <div id = "tips" >
            <p>
            Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears. </p> <
            /div>

            <div id = "options-or">
            <p className = "or"> OR </p> </div>

            </div>

            <div id = "upload-photo-section">
            <div className = "ios-pic">
            <div >
            <img className = "display-img"
            id = "pic"
            src = ""
            alt = ""/>
            <img className = "display-img-url"
            id = "photo"
            src = ""
            alt = ""/>
            </div>

            <div className = "upload-loader" > </div>

            <div >
            <label className = "fileContainer">
            Upload a document <
            input id = "browse-file"
            type = "file"
            onClick = {
                this.uploadSelfie
            }
            /> 
            </label>

            </div>

            </div> </div>

            <div className ="goback" id="front_back">
               <span className = "try-tab">
                     <a href="/#/options">Back</a>
                </span>
            </div>

            <div>
            <p id = "error-message"> </p>
            </div>

            <div>
                 <p id="success-message"></p>
            </div>
             <div>
                 <p id="success-message-1"></p>
            </div>
            <div>
                 <p id="success-message-2"></p>
            </div>

              <div id="responsetab" style={{ display: "none" }}>
        <Row className="responsecode-tab">
            <Col></Col>
            <Col>
                <pre id="responsecode" style={{ display: "none" }} className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
         <Col></Col>
         <Col id="next-step">
            <div>
               <span className = "try-tab">
                     <a href="/#/uploadselfie">Next</a>
                </span>
              </div>
               <div className ="goback">
               <span className = "try-tab">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Back</a>
                </span>
              </div>
               
          </Col>
          <Col></Col>
         </Row>
    </div>

<div id="typematch" style={{ display: "none" }}>
   <Row className="refresh-btn">
         <Col></Col>
         <Col>
             <span className = "try-tab">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Back</a>
                 </span>
          </Col>
          <Col></Col>
         </Row>
 </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

            <div id = "try-btn"
            style = {
                {
                    display: "none"
                }
            } >
            <span className = "try-tab">
            <a href = "/" > Try Again </a> 
            </span> </div>
</div>
        );
    }
}

export default Uploadkyc;