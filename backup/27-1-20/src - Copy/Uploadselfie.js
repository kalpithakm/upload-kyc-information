import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import Camera, {
    FACING_MODES,
    IMAGE_TYPES
} from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col,
    Input,
    Button
} from "reactstrap";
import {
    Link
} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";
import loader from './loader.gif';
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var APIkey;

class Uploadselfie extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

   this.state = {
        apikey: cookies.get('cookieAPIkey')
     }

     var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   console.log("APIkey", APIkey);
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

  }

    onTakePhoto(dataUri) {
        
        
        console.log("takePhoto");
        jQuery.ajax({
            url: "https://southeastasia.cognitive.sparshik.com/api/v1/facelists/addFace/",
            method: "POST",
            crossDomain: true,
            headers: {
                Authorization: APIkey,
                "Content-Type": "application/json"
            },
            data: JSON.stringify({
                dataUri: dataUri,
                attributes: "text"
            }),
            success: function(response) {
                jQuery("#error-message").hide();
                console.log(response);

                if (response) {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("idn-tab").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";
                                }

            },

            beforeSend: function() {
                jQuery('.loader').show();
                jQuery('#container-circles').hide();
                jQuery('.react-html5-camera-photo').hide();

            },
            complete: function() {
                jQuery('.loader').hide();
                jQuery('#container-circles').show();
                jQuery('.react-html5-camera-photo').show();
            },

             error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("video-section").style.display = "none";
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("idn-tab").style.display = "none";
                                document.getElementById("tips").style.display = "none";
                                document.getElementById("options-or").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }
        });
    }




    uploadSelfie(e) {
        document.getElementById("video-section").style.display = "none";
        document.getElementById("tips").style.display = "none";
        document.getElementById("options-or").style.display = "none";

        /* document.getElementById("upload-photo-section").style.display = "block";*/
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;
                        console.log(dataUri);

                        jQuery.ajax({
                            url: "https://southeastasia.cognitive.sparshik.com/api/v1/kyc/detect/",
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                 attributes: "text"
                              
                            }),

                            success: function(response) {

                                jQuery("#error-message").hide();
                                console.log(response);

                                if (response) {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                  /*  var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";*/

                                    if (response.result) 
                                      {
                                      if (response.result.length === 1) 
                                      {
                                    var c_type = response.result[0].type;
                                      console.log("ctype", c_type);
                                     var c_aadhaarno = response.result[0].details.aadhaar;
                                     console.log("aadhaarno", c_aadhaarno);
                                     var c_dob = response.result[0].details.dob;
                                     console.log("dob", c_dob);
                                     var c_doi = response.result[0].details.doi;
                                     console.log("doi", c_doi);
                                     var c_father = response.result[0].details.father;
                                     console.log("father", c_father);
                                     var c_husband = response.result[0].details.husband;
                                     console.log("husband", c_husband);
                                     var c_gender = response.result[0].details.gender;
                                     console.log("gender", c_gender);
                                     var c_mother = response.result[0].details.mother;
                                     console.log("mother", c_mother);
                                     var c_name = response.result[0].details.name;
                                     console.log("name", c_name);
                                     var c_yob = response.result[0].details.yob;
                                     console.log("yob", c_yob);
                                     var c_qr = response.result[0].details.qr;
                                     console.log("qr", c_qr);
                                     var c_addresscareof = response.result[0].details.address.care_of;
                                     console.log("address care_of", c_addresscareof);
                                     var c_district = response.result[0].details.address.district;
                                     console.log("address district", c_district);
                                     var c_sub_district = response.result[0].details.address.sub_district;
                                     console.log("address sub_district", c_sub_district);
                                     var c_locality = response.result[0].details.address.locality;
                                     console.log("address locality", c_locality);
                                      var c_landmark = response.result[0].details.address.landmark;
                                     console.log("address landmark", c_landmark);
                                      var c_street = response.result[0].details.address.street;
                                     console.log("address street", c_street);
                                      var c_vtc = response.result[0].details.address.vtc;
                                     console.log("address vtc", c_vtc);
                                     var c_house = response.result[0].details.address.house;
                                     console.log("address house", c_house);
                                     var c_pin = response.result[0].details.address.pin;
                                     console.log("address pin", c_pin);
                                     var c_post_office = response.result[0].details.address.post_office;
                                     console.log("address post_office", c_post_office);
                                     var c_state = response.result[0].details.address.state;
                                     console.log("address state", c_state);
                                     var c_value = response.result[0].details.address.value;
                                     console.log("address value", c_value);
                                     var c_phone = response.result[0].details.phone;
                                     console.log("phone", c_phone);
                                     var c_tag = response.result[0].details.tag;
                                     console.log("tag", c_tag);

                                    document.getElementById("page-title").style.display = "none";
                                       jQuery('#d_type').html(c_type);
                                       jQuery('#d_cardnumber').html(c_aadhaarno);
                                       jQuery('#d_dob').html(c_dob);
                                       jQuery('#d_doi').html(c_doi);
                                       jQuery('#d_father').html(c_father);
                                       jQuery('#d_husband').html(c_husband);
                                       jQuery('#d_gender').html(c_gender);
                                       jQuery('#d_mother').html(c_mother);
                                       jQuery('#d_name').html(c_name);
                                       jQuery('#d_yob').html(c_yob);
                                       jQuery('#d_qr').html(c_qr);
                                       jQuery('#d_addr_careof').html(c_addresscareof);
                                       jQuery('#d_addr_district').html(c_district);
                                       jQuery('#d_addr_sub_district').html(c_sub_district);
                                       jQuery('#d_addr_locality').html(c_locality);
                                       jQuery('#d_addr_landmark').html(c_landmark);
                                       jQuery('#d_addr_street').html(c_street);
                                       jQuery('#d_addr_vtc').html(c_vtc);
                                       jQuery('#d_addr_house').html(c_house);
                                       jQuery('#d_addr_pin').html(c_pin);
                                       jQuery('#d_addr_postoffice').html(c_post_office);
                                       jQuery('#d_addr_state').html(c_state);
                                       jQuery('#d_addr_value').html(c_value);
                                       jQuery('#d_phonenumber').html(c_phone);
                                       jQuery('#d_tag').html(c_tag);


                                        }
                                       }

                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("tips").style.display = "none";
                                document.getElementById("options-or").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }

    onCameraError(error) {
        console.error("onCameraError");
        document.getElementById("video-section").style.display = "none";

    }

    onCameraStart(stream) {
        console.log("onCameraStart");
    }

    onCameraStop() {
        console.log("onCameraStop");
    }

    render() {
        return ( 
            <div className = "Uploadselfie" >
            <Header />

            <div className ="page-title" id="page-title">
            <h5>Upload a Selfie / Take a Selfie</h5>
            </div>

            <div id = "video-section" >
            <Camera onTakePhoto = {
                dataUri => {
                    this.onTakePhoto(dataUri);
                }
            }
            onCameraError = {
                error => {
                    this.onCameraError(error);
                }
            }
            idealFacingMode = {
                FACING_MODES
            }
            idealResolution = {
                {
                    width: 900,
                    height: 500
                }
            }
            imageType = {
                IMAGE_TYPES.JPG
            }
            imageCompression = {
                0.50
            }
            isMaxResolution = {
                false
            }
            isImageMirror = {
                false
            }
            isDisplayStartCameraError = {
                true
            }
            sizeFactor = {
                1
            }
            onCameraStart = {
                stream => {
                    this.onCameraStart(stream);
                }
            }
            onCameraStop = {
                () => {
                    this.onCameraStop();
                }
            }
            />

            <div className = "loader"> </div>

            <div id = "tips" >
            <p>
            Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears. </p> <
            /div>

            <div id = "options-or">
            <p className = "or"> OR </p> </div>

            </div>

            <div id = "upload-photo-section">
            <div className = "ios-pic">
            <div >
            <img className = "display-img"
            id = "pic"
            src = ""
            alt = ""/>
            <img className = "display-img-url"
            id = "photo"
            src = ""
            alt = ""/>
            </div>

            <div className = "upload-loader" > </div>

            <div >
            <label className = "fileContainer">
            Upload a photo <
            input id = "browse-file"
            type = "file"
            onClick = {
                this.uploadSelfie
            }
            /> 
            </label>

            </div>

            </div> </div>

            <div>
            <p id = "error-message"> </p>
            </div>

            <div>
                 <p id="success-message"></p>
            </div>

              <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col>
             <h5> Card Details </h5>
             <table className="table" id="records_table" border='1'>
              
               <tr>
               <td>Card Type</td>
               <td id="d_type"></td>
               </tr>

               <tr>
               <td>Card Number</td>
               <td id="d_cardnumber"></td>
               </tr>

               <tr>
               <td>DOB</td>
               <td id="d_dob"></td>
               </tr>

               <tr>
               <td>DOI</td>
               <td id="d_doi"></td>
               </tr>

               <tr>
               <td>Father</td>
               <td id="d_father"></td>
               </tr>

               <tr>
               <td>Husband</td>
               <td id="d_husband"></td>
               </tr>

               <tr>
               <td>Gender</td>
               <td id="d_gender"></td>
               </tr>

               <tr>
               <td>Mother</td>
               <td id="d_mother"></td>
               </tr>

               <tr>
               <td>Name</td>
               <td id="d_name"></td>
               </tr>

               <tr>
               <td>YOB</td>
               <td id="d_yob"></td>
               </tr>

               <tr>
               <td>QR</td>
               <td id="d_qr"></td>
               </tr>

               <tr>
               <td>Address care of</td>
               <td id="d_addr_careof"></td>
               </tr>

               <tr>
               <td>Address district</td>
               <td id="d_addr_district"></td>
               </tr>

               <tr>
               <td>Address sub district</td>
               <td id="d_addr_sub_district"></td>
               </tr>

               <tr>
               <td>Address locality</td>
               <td id="d_addr_locality"></td>
               </tr>

               <tr>
               <td>Address landmark</td>
               <td id="d_addr_landmark"></td>
               </tr>

               <tr>
               <td>Address street</td>
               <td id="d_addr_street"></td>
               </tr>

               <tr>
               <td>Address vtc</td>
               <td id="d_addr_vtc"></td>
               </tr>

               <tr>
               <td>Address house</td>
               <td id="d_addr_house"></td>
               </tr>

               <tr>
               <td>Address pin</td>
               <td id="d_addr_pin"></td>
               </tr>

               <tr>
               <td>Address post office</td>
               <td id="d_addr_postoffice"></td>
               </tr>

               <tr>
               <td>Address state</td>
               <td id="d_addr_state"></td>
               </tr>

               <tr>
               <td>Address value</td>
               <td id="d_addr_value"></td>
               </tr>

               <tr>
               <td>Phone number</td>
               <td id="d_phonenumber"></td>
               </tr>

               <tr>
               <td>Tag</td>
               <td id="d_tag"></td>
               </tr>




              </table>
                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
         <Col></Col>
         <Col>
               <span className = "try-tab">
                     <a href="/#/uploadselfie" onClick={() => window.location.reload()}>Back</a>
                </span>
          </Col>
          <Col></Col>
         </Row>
    </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/uploadselfie" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

            <div id = "try-btn"
            style = {
                {
                    display: "none"
                }
            } >
            <span className = "try-tab">
            <a href = "/" > Try Again </a> 
            </span> </div>
</div>
        );
    }
}

export default Uploadselfie;