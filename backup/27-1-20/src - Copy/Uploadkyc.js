import React, {
    Component
} from "react";
import "./App.css";
import "./custom.css";
import Camera, {
    FACING_MODES,
    IMAGE_TYPES
} from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import {
    Row,
    Col,
    Input,
    Button
} from "reactstrap";
import {
    Link
} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";
import loader from './loader.gif';
import Header from './Header';
import { Cookies } from "react-cookie";
const cookies = new Cookies();
var APIkey;

class Uploadkyc extends Component {
    constructor(props) {
        super(props);
        this.uploadSelfie = this.uploadSelfie.bind(this);

  this.state = {
        apikey: cookies.get('cookieAPIkey')
     }

     var myapiCookie = cookies.get('cookieAPIkey')

    if (myapiCookie != null) 
    {
  var x = cookies.get('cookieAPIkey');
   APIkey = x.key;
   console.log("APIkey", APIkey);
  }
   else
   {
     alert("Please enter API Key");
     window.location.href='/#/editapi';
   }

  }


    onTakePhoto(dataUri) {
        // Do stuff with the dataUri photo...
        
        console.log("takePhoto");
        jQuery.ajax({
            url: "https://southeastasia.cognitive.sparshik.com/api/v1/kyc/detect/",
            method: "POST",
            crossDomain: true,
            headers: {
                Authorization: APIkey,
                "Content-Type": "application/json"
            },
            data: JSON.stringify({
                dataUri: dataUri,
                 attributes: "text"
            }),
            success: function(response) {
                jQuery("#error-message").hide();
                console.log(response);

                if (response) {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";
                                }

            },

            beforeSend: function() {
                jQuery('.loader').show();
                jQuery('#container-circles').hide();
                jQuery('.react-html5-camera-photo').hide();

            },
            complete: function() {
                jQuery('.loader').hide();
                jQuery('#container-circles').show();
                jQuery('.react-html5-camera-photo').show();
            },

             error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("video-section").style.display = "none";
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("tips").style.display = "none";
                                document.getElementById("options-or").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }
        });
    }

    uploadSelfie(e) {
        document.getElementById("video-section").style.display = "none";
        document.getElementById("tips").style.display = "none";
        document.getElementById("options-or").style.display = "none";

        /* document.getElementById("upload-photo-section").style.display = "block";*/
        jQuery(function($) {
            function readFile() {
                if (this.files && this.files[0]) {
                    var ImgfileReader = new FileReader();

                    ImgfileReader.addEventListener("load", function(e) {
                        document.getElementById("photo").src = e.target.result;
                        var dataUri = e.target.result;

                        jQuery.ajax({
                            url: "https://southeastasia.cognitive.sparshik.com/api/v1/kyc/detect/",
                            method: "POST",
                            crossDomain: true,
                            headers: {
                                Authorization: APIkey,
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify({
                                dataUri: dataUri,
                                 attributes: "text"
                              
                            }),

                            success: function(response) {

                                jQuery("#error-message").hide();
                                console.log(response);

                                if (response) {
                                    document.getElementById("video-section").style.display = "none";
                                    document.getElementById("upload-photo-section").style.display = "none";
                                    document.getElementById("tips").style.display = "none";
                                    document.getElementById("options-or").style.display = "none";
                                    document.getElementById("errorresponsetab").style.display = "none";
                                    document.getElementById("responsetab").style.display = "block";

                                    var codeString = JSON.stringify(response, null, 4);
                                    jQuery('#responsecode').html(codeString);
                                    var cardtype = response.type;
                                    var cresult = response.result;
                                    var cardresult = JSON.stringify(cresult, null, 4);
                                     console.log("result", cardresult);
                                      if (response.result) 
                                      {
                                      if (response.result.length === 1) 
                                      {
                                      var c_type = response.result[0].type;
                                      console.log("ctype", c_type);
                                     var c_aadhaarno = response.result[0].details.aadhaar;
                                     console.log("aadhaarno", c_aadhaarno);

                                    var CookieDate = new Date;
                                    CookieDate.setFullYear(CookieDate.getFullYear() +10);
                                    var expdate = CookieDate.toGMTString();
                                     console.log(expdate);
                                    cookies.set('cookieCardResponse_ctype', {key: c_type}, { path: '/' }, { expires: expdate })
                                    cookies.set('cookieCardResponse_aadhaarno', {key: c_aadhaarno}, { path: '/' }, { expires: expdate })
                                    }
                                       }

                                cookies.set('cookieCardResponse', {key: cardresult}, { path: '/' })

                                    /*var aadhaarback, aadhaarfront, pan;
                                    if (cardtype.aadhaar != 0 ||  cardtype.pan != 0  )
                                    {
                                        document.getElementById("success-message-1").innerHTML =
                                    "<h5>Your ID Matched, Please follow the next step..</h5>"; 
                                    }
                                    else
                                    {
                                       document.getElementById("success-message-2").innerHTML =
                                    "<h5>ID Not Matched, Please Upload the valid photo copy of Aadhar Card or PAN Card.</h5>";
                                     document.getElementById("typematch").style.display = "block";
                                    document.getElementById("next-step").style.display = "none";

                                    }*/
                                   /* document.getElementById("success-message").innerHTML =
                                    "<h5>Success Response</h5>";*/
                                   
                                }
                            },

                            beforeSend: function() {
                                jQuery('.upload-loader').show();
                                jQuery('#container-circles').hide();
                                jQuery('.react-html5-camera-photo').hide();

                            },
                            complete: function() {
                                jQuery('.upload-loader').hide();
                                jQuery('#container-circles').show();
                                jQuery('.react-html5-camera-photo').show();
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) 
                            {
                                document.getElementById("upload-photo-section").style.display = "none";
                                document.getElementById("tips").style.display = "none";
                                document.getElementById("options-or").style.display = "none";
                                document.getElementById("errorresponsetab").style.display = "block";
                                document.getElementById("responsetab").style.display = "none";
                                var err = eval("(" + XMLHttpRequest.responseText + ")");
                                var err_msg = err;
                                var eString = JSON.stringify(err_msg, null, 4);
                                console.log(eString);

                                jQuery('#responseerror').html(eString);
                                document.getElementById("error-message").innerHTML =
                                    "<h5>Error Response</h5>";

                            }

                        });
                    });

                    ImgfileReader.readAsDataURL(this.files[0]);
                }
            }

            document
                .getElementById("browse-file")
                .addEventListener("change", readFile);

        });
    }

    onCameraError(error) {
        console.error("onCameraError");
        document.getElementById("video-section").style.display = "none";

    }

    onCameraStart(stream) {
        console.log("onCameraStart");
    }

    onCameraStop() {
        console.log("onCameraStop");
    }

    render() {
        return ( 
            <div className = "Uploadkyc" >
            <Header />

            <div className ="page-title">
            <h5>Upload PAN or Aadhar Card / Take a Photo of PAN or Aadhar Card</h5>
            </div>

            <div id = "video-section" >
            <Camera onTakePhoto = {
                dataUri => {
                    this.onTakePhoto(dataUri);
                }
            }
            onCameraError = {
                error => {
                    this.onCameraError(error);
                }
            }
            idealFacingMode = {
                FACING_MODES
            }
            idealResolution = {
                {
                    width: 900,
                    height: 500
                }
            }
            imageType = {
                IMAGE_TYPES.JPG
            }
            imageCompression = {
                0.50
            }
            isMaxResolution = {
                false
            }
            isImageMirror = {
                false
            }
            isDisplayStartCameraError = {
                true
            }
            sizeFactor = {
                1
            }
            onCameraStart = {
                stream => {
                    this.onCameraStart(stream);
                }
            }
            onCameraStop = {
                () => {
                    this.onCameraStop();
                }
            }
            />

            <div className = "loader"> </div>

            <div id = "tips" >
            <p>
            Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears. </p> <
            /div>

            <div id = "options-or">
            <p className = "or"> OR </p> </div>

            </div>

            <div id = "upload-photo-section">
            <div className = "ios-pic">
            <div >
            <img className = "display-img"
            id = "pic"
            src = ""
            alt = ""/>
            <img className = "display-img-url"
            id = "photo"
            src = ""
            alt = ""/>
            </div>

            <div className = "upload-loader" > </div>

            <div >
            <label className = "fileContainer">
            Upload a document <
            input id = "browse-file"
            type = "file"
            onClick = {
                this.uploadSelfie
            }
            /> 
            </label>

            </div>

            </div> </div>

            <div>
            <p id = "error-message"> </p>
            </div>

            <div>
                 <p id="success-message"></p>
            </div>
             <div>
                 <p id="success-message-1"></p>
            </div>
            <div>
                 <p id="success-message-2"></p>
            </div>

              <div id="responsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responsecode" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

        <Row className="refresh-btn">
         <Col></Col>
         <Col id="next-step">
               <span className = "try-tab">
                     <a href="/#/uploadselfie">Next</a>
                </span>
               
          </Col>
          <Col></Col>
         </Row>
    </div>

<div id="typematch" style={{ display: "none" }}>
   <Row className="refresh-btn">
         <Col></Col>
         <Col>
             <span className = "try-tab">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Back</a>
                 </span>
          </Col>
          <Col></Col>
         </Row>
 </div>

    <div id="errorresponsetab" style={{ display: "none" }}>
        <Row>
            <Col></Col>
            <Col className="r-col">
                <pre id="responseerror" className="x-code sparshik-demo-pre"></pre>
            </Col>
            <Col></Col>
        </Row>

         <Row className="refresh-btn">
          <Col></Col>
           <Col>
           <span className = "try-tab">
                     <a href="/#/uploadkyc" onClick={() => window.location.reload()}>Try Again</a>
                 </span>
            </Col>
           <Col></Col>
          </Row>
    </div>

            <div id = "try-btn"
            style = {
                {
                    display: "none"
                }
            } >
            <span className = "try-tab">
            <a href = "/" > Try Again </a> 
            </span> </div>
</div>
        );
    }
}

export default Uploadkyc;